module.exports = {
  extends: ['@commitlint/config-conventional'], // 使用@commitlint/config-conventional规范
};

// feat	新增功能
// fix	bug的修复（ps: 修复线上问题）
// perf	性能优化
// refactor	重构代码(既没有新增功能，也没有修复 bug)
// build	主要目的是修改项目构建系统(例如 gulp，webpack，rollup 的配置等)的提交
// ci	主要目的是修改项目继续集成流程(例如 Travis，Jenkins，GitLab CI，Circle等)的提交
// docs	文档更新
// style	不影响程序逻辑的代码修改(修改空白字符，补全缺失的分号等)
// revert	回滚某个更早之前的提交
// chore	变更构建流程和辅助工具
// test	新增测试用例或是更新现有测试
