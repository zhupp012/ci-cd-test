# antd-pro 的项目模板

这是一个项目模板，配合 strong-cli 使用，也可以单独下载使用 2333

## 项目启动

node 环境要求 14.17.0

```
yarn dev
```

## 工具使用

1. fetch 方法使用

| API | 描述 | 非必填 |
| :-: | :-: | :-: |
| url | 接口地址 | 必填 |
| method | 请求方式 | - |
| params | 参数 | - |
| 剩余参数 | 参考 umi-requestAPI 文档：https://www.npmjs.com/package/umi-request#request-options | - |

```
import fetch from '@/utils/fetch';

export function getAuthCode(params: any) {
  return fetch({
    url: '/proxy/tutor/unauth/ltCollege/getAuthCode.htm',
    method: 'GET',
    params,
  });
}
```
