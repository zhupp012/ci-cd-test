import { globalRoleIdMap } from './const';
import cloneDeep from 'lodash/cloneDeep';
import simpleStore from '@/services/simpleStore';

// 根据id获取角色名
export function getRoleName(roleId: globalRoleIdMap) {
  return globalRoleIdMap[roleId].toLowerCase().replace(/_(\w)/g, (_, $1) => $1.toUpperCase());
}

//验证手机号正则
export const PhoneReg = /^1[3456789]\d{9}$/;

// 文件下载
export const mimeMap = {
  word: 'application/msword;application/vnd.openxmlformats-officedocument.wordprocessingml.document;',
  pdf: 'application/pdf',
  xlsx: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  zip: 'application/zip',
};

export function downloadByAElement(url: string, fileName?: string) {
  const aLink = document.createElement('a');
  aLink.href = url;
  aLink.setAttribute('download', fileName || '导出数据'); // 设置下载文件名称
  document.body.appendChild(aLink);
  aLink.click();
  document.body.appendChild(aLink);
}
interface utilsGetUrlArgs {
  path?: string;
  query?: Record<string, string | number> | undefined;
}
export function utilsGetUrl({ path = '', query = undefined }: utilsGetUrlArgs) {
  let defaultDomain = '';
  if (typeof window !== 'undefined') {
    // IE10 兼容
    defaultDomain =
      window.location.origin ||
      `${window.location.protocol}//${window.location.hostname}${
        window.location.port ? ':' + window.location.port : ''
      }`;
  }

  let url = `${defaultDomain}${path}`;
  if (query) {
    const queryKeys: string[] = Object.keys(query);
    if (queryKeys.length > 0) {
      const queryArray = queryKeys.map((key) => {
        return `${key}=${query[key]}`;
      });
      url = `${url}?${queryArray.join('&')}`;
    }
  }

  return url;
}
interface utilsNavigatorToArgs extends utilsGetUrlArgs {
  isNewTab?: boolean;
}

export function utilsNavigatorTo({
  path = '',
  query = undefined,
  isNewTab = false,
}: utilsNavigatorToArgs) {
  // 如果跳转到其他域名，且域名存在于globalDomain内，则在url中加入ticket标识
  const _query: any = cloneDeep(query) || {};
  let url = utilsGetUrl({ path, query: _query });
  // IE下使用跳转可能会出现不刷新的情况
  if (url === window.location.href) window.location.reload();
  const roleId = simpleStore.get('roleId');
  if (url.indexOf('{RName}') > -1 && roleId) {
    const roleName = getRoleName(roleId);
    url = url.replace('{RName}', roleName);
  }
  if (isNewTab) {
    window.open(url);
  } else {
    window.location.href = url;
  }
}
export const formatParams = (params: any) => {
  if (!params) return '';
  let str = '';
  Object.keys(params).forEach((key) => {
    if (params[key] || params[key] === 0) {
      if (typeof params[key] === 'object') {
        str += `&${key}=${JSON.stringify(params[key])}`;
      } else {
        str += `&${key}=${params[key]}`;
      }
    }
  });
  return str.length ? '?' + str.substr(1) : str;
};
