import type { RequestOptionsInit } from 'umi-request';
import request, { extend } from 'umi-request';
import { history } from 'umi';
import { message } from 'antd';
import JsCookie from 'js-cookie';
import simpleStore from '@/services/simpleStore';
import { getRoleName } from './utils';

const codeMessage = {
  200: '服务器成功返回请求的数据。',
  201: '新建或修改数据成功。',
  202: '一个请求已经进入后台排队（异步任务）。',
  204: '删除数据成功。',
  400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
  401: '用户没有权限（令牌、用户名、密码错误）。',
  403: '用户得到授权，但是访问是被禁止的。',
  404: '请求不存在',
  406: '请求的格式不可得。',
  410: '请求的资源被永久删除，且不会再得到的。',
  422: '当创建一个对象时，发生一个验证错误。',
  500: '服务器发生错误，请检查服务器。',
  502: '网关错误。',
  503: '服务不可用，服务器暂时过载或维护。',
  504: '网关超时。',
};

/**
 * 异常处理程序
 */
const errorHandler = (error: any) => {
  const { response = {} } = error;
  const errortext = codeMessage[response.status] || response.statusText;
  const { status } = response;
  // console.log('errorHandler', response);
  if (status === 401) {
    message.error('登录已过期，请重新登录');
    window.sessionStorage.clear();
    JsCookie.remove('ticket');
    history.push('/user/login');
    return;
  }
  // 无操作资源权限
  if (status === 403) {
    history.push('/403');
    return;
  }
  return { message: errortext, success: false, data: null };
};

// 兼容 IE
request.interceptors.response.use(async (response): Promise<any> => {
  // 文件下载
  const disposition = response.headers.get('content-disposition');
  if (disposition) {
    const splitDis = disposition.split(';');
    if (splitDis[0] === 'attachment') {
      const fileNameDefault = splitDis[1].split('filename=')[1];
      const fileNameUnicode = disposition.split('filename*=')[1];
      const filename = fileNameUnicode
        ? decodeURIComponent(fileNameUnicode.split("''")[1])
        : decodeURIComponent(fileNameDefault);
      response
        .clone()
        .blob()
        .then((blob) => {
          const link = document.createElement('a');
          link.style.display = 'none';
          link.href = URL.createObjectURL(blob);
          link.download = filename;
          link.click();
          URL.revokeObjectURL(link.href);
        });
    }
    return response;
  }
  // IE 8-9
  const data = await response.clone().text();
  //处理无权限问题，跳转登录页
  if (data.indexOf('window.top.location.href') > -1) {
    history.replace('/user/login');
    window.sessionStorage.clear();
    JsCookie.remove('ticket');
    return { status: 401, success: false, data: null };
  } else {
    const dataJson = await response.clone().json();
    if (dataJson.code === '401') {
      history.replace('/user/login');
      window.sessionStorage.clear();
      JsCookie.remove('ticket');
      return { status: 401, success: false, data: null };
    }
    return response;
  }
});
/**
 * 配置request请求时的默认参数
 */
const fetch = function ({
  url,
  method = 'get',
  _t,
  ...arg
}: {
  url: string;
  method?: string;
  _t?: boolean; //是否加时间戳
} & RequestOptionsInit) {
  const config = {
    method,
    timeout: 5000,
    ...arg,
  };
  const _request = extend({
    ...config,
    errorHandler, // 默认错误处理
  });
  let _url = !_t ? url : url + '?_t=' + new Date().getTime();
  const roleId = simpleStore.get('roleId');
  if (roleId) {
    const roleName = getRoleName(roleId);
    _url = _url.replace('{RName}', roleName);
  }

  return _request(_url).then((data) => {
    if (!data?.success) {
      if (data?.message) message.error(data.message);
      return Promise.reject(data);
    }
    return data;
  });
};

export default fetch;
