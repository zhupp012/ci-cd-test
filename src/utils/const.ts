/**
 * 公共变量定义
 **/
//默认头像
export const defaultAvatar = `https://static.leke.cn/images/home/photo.png`;
export const femaleAvatar = `https://static.leke.cn/images/home/photo-female.png`;
export const maleAvatar = `https://static.leke.cn/images/home/photo-man.png`;

// 角色表
export enum globalRoleIdMap {
  USER = 0,
  STUDENT = 100, // 学生
  TEACHER = 101, // 老师
  PARENT = 102, // 家长
  CLASS_TEACHER = 103, // 班主任
  PROVOST = 104, // 教务
  PRESIDENT = 105, // 校长
  SCHOOL_RESEARCHER = 106, // 学校教研
  COMPANYLEADER = 108,
  EDUCATIONDIRECTOR = 109,
  TECHNICAL_SUPPORT = 110, // 技术支持
  DEVOPS = 111,
  SELLER = 112, // 客户经理
  ASSIST = 113, // 助教
  OPERATION = 114, // 运营人员
  EDUCATION_FINANCE = 115,
  SUBJECT_LEADER = 121,
  STAFF = 123, // 员工
  PLATFORM_ADMIN = 200,
  SCHOOL_ADMIN = 201,
  PLATFORM_FINANCE = 300,
  SCHOOL_FINANCE = 301, // 学校财务
  INPUTER = 400,
  CHECKER = 401,
  RESEARCHER = 402,
  QUESTION_ADMIN = 403, // 教研主管
  RESEARCH_DIRECTOR = 500,
  RESEARCH_MANAGER = 501,
  RESEARCH_MAN = 502,
  COURSE_PRODUCER = 503,
  SUPERVISOR = 504,
  COURSE_MANAGER = 505,
  COURSE_CONSULTANT = 506,
  SHIKE_EDITOR = 599,
  DISTRIBUTOR = 700, // 经销商
  PERSONAL_TEACHER = 10086, // 个人老师，后端不存在，前端自己加的
}
