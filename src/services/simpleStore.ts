// 全局共享的简单数据实例
class Store {
  private state = {};
  set(key: string, value: any) {
    this.state[key] = value;
  }
  get(key: string) {
    return this.state[key];
  }
}

const simpleStore = new Store();

export default simpleStore;
